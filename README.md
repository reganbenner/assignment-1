## Background: 
This source code was originally used for my 8540 Assignment 1. 
It has been added to a repository for PROG8560 Assignment 1,
where we understand the basics of using Github and Sourcetree.
It is a simple web page built using HTML and CSS.

## Download Instructions:

## Setting up Dev:
Here's a brief intro about what a developer must do in order to start developing the project further. 
- Open Visual Studio Code
- Open terminal
- git clone https://reganbenner-admin@bitbucket.org/reganbenner/assignment-1.git .

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## License

This project code uses the MIT license.
MIT license was chosen because it is a permissive license (you can use software under the MIT license with minimal restrictions).
MIT license is a great choice because it allows for commercial reuse and modification of your work.
From a business perspective, with MIT licensing all a business needs to do is to include the copyright notice and license text as part of their release. 
Also, it is fairly straightforwad to understand. 